package com.mhrz.dev.rothis.client.desktop.launcher;

import com.mhrz.dev.rothis.client.desktop.gui.fx.launcher.boot.BootRothisDesktop;
import javafx.application.Application;


/**
 * Created by IntelliJ IDEA at Tuesday in 2019/11/26 - 3:10 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis-desktop-test
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : Launcher
 **/

public class Launcher{


    public static void main(String[] args) {
        Application.launch(BootRothisDesktop.class, args);
    }



}
