module launcher {
    exports com.mhrz.dev.rothis.client.desktop.launcher;
    requires guifx;
    requires javafx.controls;
    requires com.jfoenix;
    requires flow;
    requires javafx.fxml;
    requires java.annotation;
    requires jcip.annotations;
    requires java.sql;
    requires service;
}