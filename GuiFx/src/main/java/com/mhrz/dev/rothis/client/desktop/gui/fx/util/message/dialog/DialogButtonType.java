package com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/12/10 - 9:22 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : DialogButtonType
 **/

public enum DialogButtonType {
    YES_NO,
    OK,
    OK_CANCEL,
    YES_NO_CANCEL,
    ABORT_RETRY_IGNORE,
    RETRY_CANCEL
}
