package com.mhrz.dev.rothis.client.desktop.gui.fx.controller;

import com.jfoenix.controls.JFXButton;
import com.mhrz.dev.rothis.client.desktop.gui.fx.launcher.boot.BootRothisDesktop;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.Dialog;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.DialogMessageType;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.DialogResult;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.DialogButtonType;
import com.mhrz.dev.rothis.client.desktop.service.service.impl.UsersServiceImpl;
import com.mhrz.dev.rothis.client.desktop.service.service.iservice.IUsersService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by IntelliJ IDEA at Monday in 2019/12/09 - 11:22 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : DashboardMenuPopupViewController
 **/

public class DashboardMenuPopupViewController implements Initializable {
    @FXML
    public AnchorPane root;
    @FXML
    public JFXButton btn_sign_out;

    private IUsersService usersService;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.usersService = new UsersServiceImpl();
    }

    @FXML
    public void onBtnSignOutClick(ActionEvent event) throws IOException {
        Dialog dialog = new Dialog();
        DialogResult dialogResult = dialog.show(
                BootRothisDesktop.getPrimaryStage(),
                "خروج از سیستم",
                "خروج از سیستم و حساب کاربری",
                "آیا مایل به خروج از سیستم هستید؟ \n  شما با این کار از حساب کاربری خود خارج میشوید، آیا ادامه میدهید؟",
                DialogButtonType.YES_NO,
                DialogMessageType.WARNING);

        if (dialogResult == DialogResult.YES) {
            usersService.logOut();
            BootRothisDesktop.closeApplication();
        }
    }
}
