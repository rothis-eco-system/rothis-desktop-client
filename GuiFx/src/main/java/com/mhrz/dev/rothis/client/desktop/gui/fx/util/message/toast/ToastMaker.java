package com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.toast;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * Created by IntelliJ IDEA at Monday in 2019/12/02 - 1:22 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ToastMaker
 **/

class ToastMaker {

    public static void show(Stage ownerStage, String toastMsg, int toastDelay, int fadeInDelay, int fadeOutDelay, double Xlocation, double Ylocation) {
        Stage toastStage = new Stage();
        toastStage.initOwner(ownerStage);

        toastStage.setResizable(false);
        toastStage.setHeight(50);
        toastStage.setWidth(toastMsg.length() * 11);
        toastStage.initStyle(StageStyle.TRANSPARENT);
        toastStage.alwaysOnTopProperty();

        toastStage.setY(Ylocation);
        toastStage.setX(Xlocation - (toastMsg.length() * 5));

        Text text = new Text(toastMsg);
        text.setFont(Font.font("Sahel", 15));
        text.setFill(Color.WHITE);

        StackPane root = new StackPane(text);
        root.setStyle("-fx-background-radius: 60; -fx-background-color: rgba(0, 0, 0, 0.7); -fx-padding: 50px;");
        root.setOpacity(2);

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        toastStage.setScene(scene);
        toastStage.show();

        Timeline fadeInTimeline = new Timeline();
        KeyFrame fadeInKey1 = new KeyFrame(Duration.millis(fadeInDelay), new KeyValue(toastStage.getScene().getRoot().opacityProperty(), 1));
        fadeInTimeline.getKeyFrames().add(fadeInKey1);
        fadeInTimeline.setOnFinished(run -> handleThread(toastDelay, fadeOutDelay, toastStage));
        fadeInTimeline.play();
    }

    private static void handleThread(int toastDelay, int fadeOutDelay, Stage toastStage) {
        new Thread(() -> {
            try {
                Thread.sleep(toastDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Timeline fadeOutTimeline = new Timeline();
            KeyFrame fadeOutKey1 = new KeyFrame(Duration.millis(fadeOutDelay), new KeyValue(toastStage.getScene().getRoot().opacityProperty(), 0));
            fadeOutTimeline.getKeyFrames().add(fadeOutKey1);
            fadeOutTimeline.setOnFinished((aeb) -> toastStage.close());
            fadeOutTimeline.play();
        }).start();
    }
}
