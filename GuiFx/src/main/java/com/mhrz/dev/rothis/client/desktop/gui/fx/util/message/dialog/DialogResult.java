package com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/12/10 - 9:20 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : DialogResult
 **/

public enum DialogResult {
    OK,
    YES,
    NO,
    CANCEL, ABORT, IGNORE, RETRAY;

    DialogResult() {
    }
}
