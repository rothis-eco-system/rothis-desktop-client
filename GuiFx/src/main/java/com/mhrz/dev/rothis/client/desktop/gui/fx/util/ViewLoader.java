package com.mhrz.dev.rothis.client.desktop.gui.fx.util;

import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.mhrz.dev.rothis.client.desktop.gui.fx.launcher.boot.BootRothisDesktop;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.Dialog;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.DialogMessageType;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.DialogResult;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog.DialogButtonType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.BuilderFactory;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA at Friday in 2019/03/29 - 8:01 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name    : Diamond
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name      : ViewLoader
 **/

public class ViewLoader {
    private final String fxml;
    private final String windowTitle;
    private final double windowHeight;
    private final double windowWeight;
    private static Stage stage;
    private final Boolean resizableMode;
    private final Window ownerWindow;
    private final boolean alwaysOnTop;
    private final boolean maximized;
    private static BuilderFactory builderFactory;
    private static ImageView appIcon;

    public ViewLoader(
            String fxml,
            String windowTitle,
            double windowWeight,
            double windowHeight,
            Stage stage,
            boolean resizableMode,
            Window ownerWindow,
            boolean alwaysOnTop, boolean maximized) {
        this.fxml = fxml;
        this.windowTitle = windowTitle;
        this.windowHeight = windowHeight;
        this.windowWeight = windowWeight;
        ViewLoader.stage = BootRothisDesktop.getPrimaryStage();
        this.resizableMode = resizableMode;
        this.ownerWindow = ownerWindow;
        this.alwaysOnTop = alwaysOnTop;
        this.maximized = maximized;
        init();
    }

    private void init() {
        appIcon = new ImageView(getClass().getResource("/assets/icons/rothis/rothis-icon-32x32.png").toExternalForm());
        // decorator.setGraphic(new ImageView("/assets/icons/rothis/rothis-icon-32x32.png"));
        // decorator.setMaximized(false);
        // Scene scene = new Scene(decorator, windowWeight, windowHeight);
        // stage.initModality(modality);
        stage.setTitle(windowTitle);
        // stage.setScene(scene);
        stage.setResizable(resizableMode);
        stage.setMinHeight(windowHeight);
        stage.setMinWidth(windowWeight);
        stage.centerOnScreen();
        stage.getIcons().add(new Image(appIcon.getImage().getUrl()));
        //stage.setMaximized(maximized);
        // stage.initOwner(ownerWindow);
    }

    public void show() {
        String style = getClass().getResource("/assets/css/decorator_style.css").toExternalForm();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource(fxml));
            Parent parent = loader.load();
            builderFactory = loader.getBuilderFactory();
            JFXDecorator decorator = new JFXDecorator(stage, parent);
            decorator.setCustomMaximize(resizableMode);
            decorator.setTitle(windowTitle);
            decorator.setGraphic(appIcon);
            decorator.setMaximized(false);
            decorator.setOnCloseButtonAction(new Runnable() {
                @Override
                public void run() {
                    Dialog dialog = new Dialog();
                    DialogResult dialogResult = dialog.show(BootRothisDesktop.getPrimaryStage(), "خروج از سیستم", "خروج از سیستم", "آیا مایل به خروج از سیستم هستید؟", DialogButtonType.YES_NO, DialogMessageType.WARNING);
                    if (dialogResult == DialogResult.YES) {
                        BootRothisDesktop.closeApplication();
                    }
                }
            });
            Scene scene = new Scene(decorator);
            scene.getStylesheets().add(style);
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setIconified(true);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showOverlayDialog() {
        try {
            JFXDialogLayout content = new JFXDialogLayout();
            Parent parent = FXMLLoader.load(getClass().getResource(fxml));
            content.setStyle("-fx-background-color: transparent;");
            BorderPane headerContent = new BorderPane();
            headerContent.setCenter(new Text(windowTitle));
            content.setHeading(headerContent);
            content.setBody(parent);
            content.setStyle("-fx-font-family: Sahel; -fx-text-alignment: left;");
            StackPane stackPane = new StackPane();
            stackPane.setStyle("-fx-background-color: rgba(0,255,255,0);");
            stage.initStyle(StageStyle.TRANSPARENT);
            Scene scene = new Scene(stackPane);
            JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
            stage.setScene(ViewUtils.getShadowScene(scene.getRoot()));
            dialog.setOverlayClose(false);
            stage.centerOnScreen();
            stage.setMaximized(true);
            stage.show();
            dialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

  /*  public void showDialog() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource(fxml));
            JFXDecorator decorator = new JFXDecorator(stage, parent);
            decorator.setCustomMaximize(resizableMode);
            decorator.setTitle(windowTitle);
            // decorator.setGraphic(new ImageView("/assets/icons/rothis/rothis-icon-32x32.png"));
            decorator.setMaximized(false);
            Scene scene = new Scene(decorator);
            stage.setScene(scene);
            // stage.initStyle(StageStyle.DECORATED);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

}
