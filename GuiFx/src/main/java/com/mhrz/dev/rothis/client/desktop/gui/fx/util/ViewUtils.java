package com.mhrz.dev.rothis.client.desktop.gui.fx.util;

import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/11 - 6:48 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : f
 **/

public class ViewUtils {
    @SuppressWarnings("Duplicates")
    public static Scene getShadowScene(Parent p) {
        Scene scene;
        if (p instanceof AnchorPane) {
            AnchorPane outer = new AnchorPane();
            outer.getChildren().add(p);
            outer.setPadding(new Insets(10.0d));
            outer.setBackground(new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0), new CornerRadii(0), new
                    Insets(0))));
            DropShadow dropShadow = new DropShadow(20, Color.color(0.4, 0.5, 0.5));
            p.setEffect(dropShadow);
            ((AnchorPane) p).setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(0), new Insets(0)
            )));
            scene = new Scene(outer);
            scene.setFill(Color.rgb(0, 255, 0, 0));
            return scene;
        } else if (p instanceof StackPane) {
            StackPane outer = new StackPane();
            outer.getChildren().add(p);
            outer.setPadding(new Insets(10.0d));
            outer.setBackground(new Background(new BackgroundFill(Color.rgb(0, 0, 0, 0), new CornerRadii(0), new
                    Insets(0))));
            DropShadow dropShadow = new DropShadow(20, Color.color(0.4, 0.5, 0.5));
            p.setEffect(dropShadow);
            ((StackPane) p).setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(0), new Insets(0)
            )));
            scene = new Scene(outer);
            scene.setFill(Color.rgb(0, 255, 0, 0));
            return scene;
        }
        return null;
    }

}
