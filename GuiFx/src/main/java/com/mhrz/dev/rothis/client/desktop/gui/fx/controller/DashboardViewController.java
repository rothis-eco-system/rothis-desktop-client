package com.mhrz.dev.rothis.client.desktop.gui.fx.controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.controlsfx.control.PopOver;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by IntelliJ IDEA at Thursday in 2019/12/05 - 12:57 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : DashboardViewController
 **/

public class DashboardViewController implements Initializable {

    @FXML
    public AnchorPane root;
    @FXML
    public StackPane stkp_root;
    @FXML
    public JFXButton btn_dashboard_menu;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    @FXML
    public void btnOnDashboardMenuClick(ActionEvent event) throws IOException {
        FXMLLoader popupMenuLoader = new FXMLLoader();
        popupMenuLoader.setLocation(getClass().getResource("/view/dashboard/dashboard_menu_popup_view.fxml"));
        AnchorPane root = popupMenuLoader.load();
        PopOver popOver = new PopOver(root);
        popOver.setContentNode(root);
        popOver.setArrowLocation(PopOver.ArrowLocation.TOP_RIGHT);
        if (!popOver.isShowing())
            popOver.show(btn_dashboard_menu);
    }

}
