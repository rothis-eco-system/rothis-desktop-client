package com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog;

import com.mhrz.dev.rothis.client.desktop.gui.fx.controller.MessageDialogViewController;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.ViewUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/12/10 - 9:19 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : Dialog
 **/

public class Dialog {
    private Stage stage;

    public Dialog() {
        this.stage = new Stage();
    }

    public DialogResult show(Stage ownerStage, String headerTitle, String messageTitle, String messageText, DialogButtonType dialogButtonType, DialogMessageType dialogMessageType) {
        DialogResult result = null;
        try {
            if (!stage.isShowing())
                if (stage.getOwner() == null) {
                    FXMLLoader dialogLoader = new FXMLLoader();
                    dialogLoader.setLocation(Dialog.class.getResource("/view/message/message_dialog_view.fxml"));
                    AnchorPane root = dialogLoader.load();
                    MessageDialogViewController dialogViewController = dialogLoader.getController();
                    dialogViewController.lbl_header_title.setText(headerTitle);
                    dialogViewController.lbl_msg_title.setText(messageTitle);
                    dialogViewController.lbl_msg_text.setText(messageText);
                    dialogViewController.setDialogButtonType(dialogButtonType);
                    dialogViewController.setDialogMessageType(dialogMessageType);
                    dialogViewController.initDialog();
                    stage.setAlwaysOnTop(true);
                    stage.initOwner(ownerStage);
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.initStyle(StageStyle.TRANSPARENT);
                    stage.setScene(ViewUtils.getShadowScene(root));
                    stage.showAndWait();
                    result = !stage.isShowing() ? dialogViewController.getDialogResult() : null;
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
