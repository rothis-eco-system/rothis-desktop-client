package com.mhrz.dev.rothis.client.desktop.gui.fx.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.mhrz.dev.rothis.client.desktop.domain.model.request.UsersLoginRequest;
import com.mhrz.dev.rothis.client.desktop.gui.fx.util.ViewLoader;
import com.mhrz.dev.rothis.client.desktop.service.service.impl.UsersServiceImpl;
import com.mhrz.dev.rothis.client.desktop.service.service.iservice.IUsersService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import static com.mhrz.dev.rothis.client.desktop.gui.fx.util.animation.Animation.fadeIn;
import static com.mhrz.dev.rothis.client.desktop.gui.fx.util.animation.Animation.fadeOut;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/11/26 - 10:52 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis-desktop-test
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : LoginViewController
 **/

public class LoginViewController implements Initializable {
    @FXML
    public AnchorPane root;

    @FXML
    public StackPane stkp_root;

    @FXML
    public HBox hbx_main_card;

    @FXML
    public StackPane stkp_user_login_signup;

    @FXML
    public VBox vb_verification_and_passwords;

    @FXML
    public Hyperlink hl_resend_verification_link;

    @FXML
    public Label lbl_expire_time;

    @FXML
    public JFXTextField tat_received_email_verification_code;

    @FXML
    public JFXPasswordField txt_new_password;

    @FXML
    public ImageView imgv_show_password;

    @FXML
    public JFXPasswordField txt_repeat_new_password;

    @FXML
    public JFXButton btn_create_account_next;

    @FXML
    public JFXButton btn_create_account_previous;

    @FXML
    public VBox vb_login;

    @FXML
    public ImageView imgv_profile;

    @FXML
    public JFXTextField txt_user_name;

    @FXML
    public JFXTextField txt_password;

    @FXML
    public JFXButton btn_exit;

    @FXML
    public JFXButton btn_login;

    @FXML
    public Hyperlink hl_sign_up;

    @FXML
    public VBox vb_sign_up_new_user;

    @FXML
    public ImageView imgv_sign_up_user_profile;

    @FXML
    public JFXTextField txt_nick_name;

    @FXML
    public JFXTextField txt_sign_up_user_name;

    @FXML
    public JFXTextField txt_email;

    @FXML
    public JFXButton btn_user_sign_up_next;

    @FXML
    public Hyperlink hl_already_have_account;

    @FXML
    public VBox vb_password_reset;

    @FXML
    public JFXTextField txt_recover_password_email;
    @FXML
    public JFXButton btn_exit_login;

    private IUsersService usersService;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.usersService = new UsersServiceImpl();
    }

    @FXML
    public void btnOnLoginClick(ActionEvent event) throws InterruptedException, IOException {
        UsersLoginRequest usersLoginRequest = new UsersLoginRequest();
        usersLoginRequest.setEmail(txt_user_name.getText().trim());
        usersLoginRequest.setPassword(txt_password.getText().trim());
        usersService.userLogin(usersLoginRequest);
        this.root.getScene().getWindow().hide();
        TimeUnit.MILLISECONDS.sleep(500);
        if (usersService.isLoggedIn()) {
            new ViewLoader(
                    "/view/dashboard/dashboard_view.fxml",
                    "سیستم جامع مدیریت منابع کشاورزی",
                    1290,
                    830,
                    new Stage(),
                    true,
                    root.getScene().getWindow(),
                    true,
                    true)
                    .show();
        } else
            Platform.exit();
    }

    @FXML
    public void onBtnCreateAccountNextClick(ActionEvent event) {
        handelStpUserSignUp(event);
    }

    @FXML
    public void onBtnCreateAccountPreviousClick(ActionEvent event) {
        handelStpUserSignUp(event);

    }

    @FXML
    public void onBtnExitClick(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    public void onBtnUserSignUpNextClick(ActionEvent event) {
        handelStpUserSignUp(event);

    }

    @FXML
    public void onHlResendVerificationLinkClick(ActionEvent event) {
        handelStpUserSignUp(event);
    }

    @FXML
    public void onHlSignUp(ActionEvent event) {
        handelStpUserSignUp(event);
    }


    @FXML
    public void onMouseShowPasswordClick(MouseEvent event) {

    }

    @FXML
    public void onHlAlreadyHaveAccountClick(ActionEvent event) {
        handelStpUserSignUp(event);

    }

    @FXML
    public void onMouseNewImageProfileSetClick(MouseEvent event) {

    }

    private void handelStpUserSignUp(ActionEvent event) {
        if (event.getSource() == hl_sign_up) {
            vb_sign_up_new_user.toFront();
            fadeIn(vb_login);
            fadeOut(vb_sign_up_new_user);
        } else if (event.getSource() == hl_already_have_account) {
            vb_login.toFront();
            fadeOut(vb_login);
        } else if (event.getSource() == btn_user_sign_up_next) {
            vb_verification_and_passwords.toFront();
            fadeOut(vb_verification_and_passwords);
        } else if (event.getSource() == btn_create_account_previous) {
            vb_sign_up_new_user.toFront();
            fadeOut(vb_sign_up_new_user);
        }
    }
}
