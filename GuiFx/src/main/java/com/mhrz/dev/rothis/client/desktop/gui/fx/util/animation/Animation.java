package com.mhrz.dev.rothis.client.desktop.gui.fx.util.animation;

import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.scene.Node;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.util.Duration;

/**
 * Created by IntelliJ IDEA at Tuesday in 2019/12/03 - 12:50 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : Animation
 **/

public class Animation {
    public static void fadeOut(Node node) {
        FadeTransition fade = new FadeTransition();
        fade.setDuration(Duration.millis(800));
        fade.setAutoReverse(true);
        fade.setNode(node);
        fade.setFromValue(0);
        fade.setToValue(100);
        fade.play();
    }

    private static void rotate(Node node, boolean revers) {
        RotateTransition rotate = new RotateTransition();
        rotate.setAxis(Rotate.Z_AXIS);
        rotate.setByAngle(360);
        rotate.setCycleCount(1);
        rotate.setDuration(Duration.millis(500));
        rotate.setAutoReverse(revers);
        rotate.setNode(node);
        rotate.play();
    }

    public static void fadeIn(Node node) {
        FadeTransition fade = new FadeTransition();
        fade.setDuration(Duration.millis(100));
        fade.setAutoReverse(true);
        fade.setNode(node);
        fade.setFromValue(100);
        fade.setToValue(0);
        fade.play();
        fade.stop();
    }

    private static void zoomIn(Node node) {
        Scale scale = new Scale();
        scale.setX(2);
        scale.setY(2);
        scale.setPivotX(node.getLayoutX()*2);
        scale.setPivotY(node.getLayoutY()*2);
        node.getTransforms().add(scale);
    }

    private static void zoomOut(Node node) {
        Scale scale = new Scale();
        scale.setX(-2);
        scale.setY(-2);
        scale.setPivotX(node.getLayoutX()/2);
        scale.setPivotY(node.getLayoutY()/2);
        node.getTransforms().add(scale);
    }
}
