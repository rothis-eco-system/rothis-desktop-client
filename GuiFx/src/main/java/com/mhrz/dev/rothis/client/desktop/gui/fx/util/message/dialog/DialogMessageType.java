package com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/11 - 7:28 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : DialogMessageType
 **/

public enum DialogMessageType {
    INFORMATION,
    WARNING,
    DANGER,
    SUCCESS
}
