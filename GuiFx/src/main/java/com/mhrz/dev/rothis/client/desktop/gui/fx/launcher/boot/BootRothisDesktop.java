package com.mhrz.dev.rothis.client.desktop.gui.fx.launcher.boot;

import com.mhrz.dev.rothis.client.desktop.gui.fx.util.ViewLoader;
import com.mhrz.dev.rothis.client.desktop.service.service.impl.UsersServiceImpl;
import com.mhrz.dev.rothis.client.desktop.service.service.iservice.IUsersService;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/11 - 10:20 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : BootRothisDesktop
 **/

public class BootRothisDesktop extends Application {

    private static Stage primaryStage;

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    public void init() {

    }

    @Override
    public void stop() {
        closeApplication();
    }


    private IUsersService usersService;

    @Override
    public void start(Stage stage) throws Exception {
        primaryStage = stage;
        usersService = new UsersServiceImpl();
        if (!usersService.isLoggedIn()) {
            new ViewLoader(
                    "/view/dashboard/dashboard_view.fxml",
                    "سیستم جامع مدیریت منابع کشاورزی",
                    1290,
                    830,
                    primaryStage,
                    true,
                    null,
                    true,
                    true)
                    .show();
        } else {
            new ViewLoader(
                    "/view/login_view.fxml",
                    "سیستم جامع مدیریت منابع کشاورزی",
                    1290,
                    830,
                    primaryStage,
                    true,
                    null,
                    true,
                    true)
                    .showOverlayDialog();
        }


    }

    public static void closeApplication() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Platform.exit();
            }
        });
    }
}
