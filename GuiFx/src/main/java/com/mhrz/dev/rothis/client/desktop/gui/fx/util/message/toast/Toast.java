package com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.toast;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Created by IntelliJ IDEA at Monday in 2019/12/02 - 1:26 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : Toast
 **/

public class Toast extends ToastMaker {

    public static void show(Stage stage, String toastMsgText, int toastMsgTime, int fadeInTime, int fadeOutTime) {
        handle(stage, toastMsgText, toastMsgTime, fadeInTime, fadeOutTime);
    }

    public static void show(Object object, String toastMsgText, int toastMsgTime, int fadeInTime, int fadeOutTime) {
        AnchorPane root = ((AnchorPane) object);
        Stage stage = (Stage) (root.getScene().getWindow());
        handle(stage, toastMsgText, toastMsgTime, fadeInTime, fadeOutTime);
    }

    public static void show(Object object, String toastMsgText) {
        AnchorPane root = ((AnchorPane) object);
        Stage stage = (Stage) root.getScene().getWindow();
        int toastMsgTime = 3500; //3.5 seconds
        int fadeInTime = 500; //0.5 seconds
        int fadeOutTime = 500; //0.5 seconds
        handle(stage, toastMsgText, toastMsgTime, fadeInTime, fadeOutTime);
    }

    public static void show(Stage stage, String toastMsgText) {
        int toastMsgTime = 3500; //3.5 seconds
        int fadeInTime = 500; //0.5 seconds
        int fadeOutTime = 500; //0.5 seconds
        handle(stage, toastMsgText, toastMsgTime, fadeInTime, fadeOutTime);
    }

    private static void handle(Stage stage, String toastMsgText, int toastMsgTime, int fadeInTime, int fadeOutTime) {
        double x = stage.getX() + stage.getWidth() / 2;
        double y = stage.getY() + stage.getHeight() - 100;
        ToastMaker.show(stage, toastMsgText, toastMsgTime, fadeInTime, fadeOutTime, x, y);
    }
}
