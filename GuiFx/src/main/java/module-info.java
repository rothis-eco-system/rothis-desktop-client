module guifx {
    exports com.mhrz.dev.rothis.client.desktop.gui.fx.controller;
    exports com.mhrz.dev.rothis.client.desktop.gui.fx.util;
    exports com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.toast;
    exports com.mhrz.dev.rothis.client.desktop.gui.fx.launcher.boot;
    exports com.mhrz.dev.rothis.client.desktop.gui.fx.util.message.dialog;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.controls;
    requires com.jfoenix;
    requires service;
    requires domain;
    requires security;
    requires org.controlsfx.controls;
}