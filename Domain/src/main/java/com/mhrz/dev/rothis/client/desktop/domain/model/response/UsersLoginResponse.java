package com.mhrz.dev.rothis.client.desktop.domain.model.response;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/11/06 - 11:14 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UsersLoginResponse
 **/

import java.io.Serializable;

public class UsersLoginResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private final String token;

    public UsersLoginResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }
}