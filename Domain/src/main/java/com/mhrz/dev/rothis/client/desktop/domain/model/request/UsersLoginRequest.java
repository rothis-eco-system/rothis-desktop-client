package com.mhrz.dev.rothis.client.desktop.domain.model.request;

/**
 * Created by IntelliJ IDEA at Sunday in 2019/11/03 - 9:05 AM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : Rothis
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UserLoginDetailsRequest
 **/
public class UsersLoginRequest {
    private String email;
    private String password;

    public UsersLoginRequest() {

    }

    public UsersLoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UsersLoginRequest{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
