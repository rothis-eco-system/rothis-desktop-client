module service {
    requires domain;
    requires retrofit2;
    requires retrofit2.converter.gson;
    requires okhttp3;
    requires annotations;
    requires okhttp3.logging;
    requires com.google.gson;
    requires security;
    exports com.mhrz.dev.rothis.client.desktop.service.service.iservice;
    exports com.mhrz.dev.rothis.client.desktop.service.service.impl;
}