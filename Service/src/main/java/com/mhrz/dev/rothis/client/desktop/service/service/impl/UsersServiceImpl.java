package com.mhrz.dev.rothis.client.desktop.service.service.impl;

import com.mhrz.dev.rothis.client.desktop.domain.model.request.UsersLoginRequest;
import com.mhrz.dev.rothis.client.desktop.domain.model.response.UsersLoginResponse;
import com.mhrz.dev.rothis.client.desktop.security.session.ISession;
import com.mhrz.dev.rothis.client.desktop.security.session.SessionImpl;
import com.mhrz.dev.rothis.client.desktop.service.service.api.RothisAPI;
import com.mhrz.dev.rothis.client.desktop.service.service.api.routes.IUsersAPIService;
import com.mhrz.dev.rothis.client.desktop.service.service.iservice.IUsersService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/04 - 5:55 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : UsersServiceImpl
 **/

public class UsersServiceImpl implements IUsersService {
    private ISession session = new SessionImpl();

    @Override
    public void userLogin(UsersLoginRequest loginRequest) {
        RothisAPI rothisAPI = new RothisAPI.Builder().build();
        IUsersAPIService iUsersAPIService = rothisAPI.getRetrofit().create(IUsersAPIService.class);
        Call<UsersLoginResponse> call = iUsersAPIService.loginUser(loginRequest);
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<UsersLoginResponse> call, Response<UsersLoginResponse> response) {
                if (!response.isSuccessful()) {
                    System.out.println("Code :" + response.code() + " " + response.message());
                } else {
                    if (response.body() != null) {
                        if (response.code() == 200) {
                            session.saveToken(response.body().getToken());
                        }else {
                            session.invalidate();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UsersLoginResponse> call, Throwable throwable) {
                System.out.println("catch error : " + throwable.getMessage());
            }
        });
    }

    @Override
    public boolean isLoggedIn() {
        return session.isLoggedIn();
    }

    @Override
    public void logOut() {
        session.logOut();
    }
}
