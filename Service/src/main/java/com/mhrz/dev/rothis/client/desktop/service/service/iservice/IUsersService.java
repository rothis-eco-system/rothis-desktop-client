package com.mhrz.dev.rothis.client.desktop.service.service.iservice;

import com.mhrz.dev.rothis.client.desktop.domain.model.request.UsersLoginRequest;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/04 - 5:54 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IUsersService
 **/

public interface IUsersService {
    void userLogin(UsersLoginRequest dto);
    boolean isLoggedIn();
    void logOut();
}
