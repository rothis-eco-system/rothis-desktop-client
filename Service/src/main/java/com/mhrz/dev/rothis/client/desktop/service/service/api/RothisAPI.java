package com.mhrz.dev.rothis.client.desktop.service.service.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import org.jetbrains.annotations.NotNull;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/04 - 12:14 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : testRothisFunctions
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : APIBuilder
 **/

public class RothisAPI {
    private final static String SERVER_URL = "http://127.0.0.1:8081";
    private final static String VERSION = "/api/v1/";
    private final static String ROTHIS_URL = SERVER_URL + VERSION;
    private static final int DISK_CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    private final Retrofit retrofit;


    public static class Builder {
        private final Retrofit retrofit;

        public Builder() {

            this.retrofit = new Retrofit.Builder()
                    .baseUrl(ROTHIS_URL)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .client(getOkHttpClient())
                    .build();
        }

        public Retrofit getRetrofit() {
            return retrofit;
        }

        public RothisAPI build() {
            return new RothisAPI(
                    this);
        }

        private Gson getGson() {
            return new GsonBuilder().serializeNulls().create();
        }

        private OkHttpClient getOkHttpClient() {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder okHttp = new OkHttpClient.Builder();

            okHttp.addInterceptor(new Interceptor() {
                @NotNull
                @Override
                public Response intercept(@NotNull Chain chain) throws IOException {
                    Request originalRequest = chain.request();
                    Request newRequest = originalRequest.newBuilder().header("Accept", "application/json").header("Content-Type", "application/json").build();
                    return chain.proceed(newRequest);
                }
            })
                    .addInterceptor(httpLoggingInterceptor)
                    .build();
            okHttp.connectTimeout(30, TimeUnit.SECONDS);
            okHttp.readTimeout(30, TimeUnit.SECONDS);
            okHttp.writeTimeout(30, TimeUnit.SECONDS);
            okHttp.cache(getCache());
            return okHttp.build();
        }

        private Cache getCache() {
            File cacheDir = new File(getCacheDir(), "cache");
            return new Cache(cacheDir, DISK_CACHE_SIZE);
        }

        private File getCacheDir() {
            return new File(new File("cache").getAbsolutePath());
        }
    }

    private RothisAPI(Builder builder) {
        this.retrofit = builder.retrofit;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

}
