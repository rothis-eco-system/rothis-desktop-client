package com.mhrz.dev.rothis.client.desktop.service.service.api.routes;

import com.mhrz.dev.rothis.client.desktop.domain.model.request.UsersLoginRequest;
import com.mhrz.dev.rothis.client.desktop.domain.model.response.UsersLoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/04 - 12:02 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : testRothisFunctions
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : IUsersAPIService
 **/

public interface IUsersAPIService {
    @POST(value = "users/login")
    Call<UsersLoginResponse> loginUser(@Body UsersLoginRequest loginRequest);
}
