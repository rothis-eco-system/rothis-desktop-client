package com.mhrz.dev.rothis.client.desktop.security.session;

import java.util.Base64;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/04 - 10:50 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : SessionImpl
 **/

public class SessionImpl implements ISession {

    private final static String TOKEN_KEY = "token";
    private final static String TOKEN_PREFIX = "Bearer ";
    private static final String EMAIL_KEY = "email";
    private static final String PASSWORD_KEY = "password";
    private static final String TOKEN_EXPIRATION_KEY = "tokenExpirationTime";
    private static final String MANAGER_PUBLIC_ID_KEY = "managerPublicId";
    private static final String RESOURCE_PUBLIC_ID_KEY = "resourcePublicId";

    private Preferences getPreferences() {
        return Preferences.userNodeForPackage(this.getClass());
    }

    @Override
    public boolean isLoggedIn() {
        return !getPreferences().get(TOKEN_KEY, "").isEmpty();
    }

    @Override
    public void saveToken(String token) {
        getPreferences().put(TOKEN_KEY, encode(TOKEN_PREFIX + token));
        System.out.println(getToken());
    }

    @Override
    public String getToken() {
        return decode(getPreferences().get(TOKEN_KEY, ""));
    }

    @Override
    public void saveEmail(String email) {
        getPreferences().put(EMAIL_KEY, encode(email));
    }

    @Override
    public String getEmail() {
        return decode(getPreferences().get(EMAIL_KEY, ""));
    }

    @Override
    public void savePassword(String password) {
        getPreferences().put(PASSWORD_KEY, encode(password));
    }

    @Override
    public String getPassword() {
        return decode(getPreferences().get(PASSWORD_KEY, ""));
    }

    @Override
    public void invalidate() {
    }

    @Override
    public void logOut() {
        try {
            getPreferences().clear();
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveManagerPublicId(String managerPublicId) {
        getPreferences().put(MANAGER_PUBLIC_ID_KEY, managerPublicId);
    }

    @Override
    public String getManagerPublicId() {
        return getPreferences().get(MANAGER_PUBLIC_ID_KEY, "");
    }

    @Override
    public void saveResourcePublicId(String resourcePublicId) {
        getPreferences().put(RESOURCE_PUBLIC_ID_KEY, encode(resourcePublicId));
    }

    @Override
    public String getResourcePublicId() {
        return decode(getPreferences().get(RESOURCE_PUBLIC_ID_KEY, ""));
    }

    public void setTokenExpirationTime() {
        getPreferences().putInt(TOKEN_EXPIRATION_KEY, 10 * 24 * 60 * 60 * 1000);
    }

    private String encode(String normalStr) {
        return Base64.getEncoder().encodeToString(normalStr.getBytes());
    }

    private String decode(String codedStr) {
        byte[] bytes = Base64.getDecoder().decode(codedStr);
        return new String(bytes);
    }
}
