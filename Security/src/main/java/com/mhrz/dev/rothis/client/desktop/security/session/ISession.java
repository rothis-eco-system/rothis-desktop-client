package com.mhrz.dev.rothis.client.desktop.security.session;

/**
 * Created by IntelliJ IDEA at Wednesday in 2019/12/04 - 10:49 PM
 * Copyright (c) 2019 MoHRaZ
 * Project Name        : rothis.desktop
 *
 * @Author : Mohammad Hadi Rafiei Zadeh
 * @Email : mhrz.dev@gmail.com
 * Class Name          : ISession
 **/

public interface ISession {
    boolean isLoggedIn();
    void saveToken(String token);
    String getToken();
    void saveEmail(String email);
    String getEmail();
    void savePassword(String password);
    String getPassword();
    void invalidate();
    void logOut();
    void saveManagerPublicId(String managerPublicId);
    String getManagerPublicId();
    void saveResourcePublicId(String resourcePublicId);
    String getResourcePublicId();
}
